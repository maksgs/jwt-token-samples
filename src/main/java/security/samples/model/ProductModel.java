package security.samples.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductModel {

	private Integer modelId;
	private ProductFamily productFamily;
}
