package security.samples.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum InformationType {
	ONE (0),
	TWO(1),
	THREE(2);

	@Getter
	private int order;

}
