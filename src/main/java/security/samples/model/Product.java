package security.samples.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {

	private String prefix;
	private Integer modelId;
	private String productFamily;

}
