package security.samples.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Define response body for error responses
 */
@Setter
@Getter
@AllArgsConstructor
public class ApplicationErrorResponse {

	private String message;

	private Object details;

	public ApplicationErrorResponse(String message) {
		this.message = message;
	}

	public ApplicationErrorResponse(Exception ex) {
		this.message = ex.getMessage();
	}
}
