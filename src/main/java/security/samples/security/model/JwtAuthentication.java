package security.samples.security.model;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtAuthentication implements Authentication {

	private final JwtUser principal;

	/**
	 * Creates a token with the supplied array of authorities.
	 */
	public JwtAuthentication(JwtUser jwtUser) {
		this.principal = jwtUser;
	}


	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	public Object getCredentials() {
		return null;
	}

	public Object getDetails() {
		return null;
	}

	public JwtUser getPrincipal() {
		return principal;
	}

	public boolean isAuthenticated() {
		return true;
	}

	public void setAuthenticated(boolean b) throws IllegalArgumentException {

	}

	public String getName() {
		return null;
	}
}
