package security.samples.security.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * represents security object
 */
@Getter
@Setter
@AllArgsConstructor
public class JwtUser {

	private String cwsId;

	//each bit in binary representation represents if user has access to category or not
	private Integer informationTypes;

	private List<String> productFamilies;

	private List<String> productModels;

	private List<String> productPrefixes;

	//looks like not needed, if family is excluded, just no need to include it white list
	private List<String> productFamiliesExclusions;

	private List<String> productModelsExclusions;

	private List<String> productPrefixesExclusions;
}
