package security.samples.security.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtToken {
	private String accessToken;
	//todo: define if needed
	private String refreshToken;
}
