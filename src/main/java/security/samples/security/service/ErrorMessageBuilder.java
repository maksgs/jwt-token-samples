package security.samples.security.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class ErrorMessageBuilder {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String getJsonErrorMessage(String message) {
        try {
            return OBJECT_MAPPER.writeValueAsString(new MessageHolder(message));
        } catch (IOException e) {
            log.error("Exception while map message holder");
            throw new RuntimeException(e.getMessage());
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    private static class MessageHolder{
        private String message;
    }
}
