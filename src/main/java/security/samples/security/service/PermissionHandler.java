package security.samples.security.service;

import security.samples.model.InformationType;
import security.samples.model.ProductFamily;
import security.samples.security.model.JwtUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * this component contains method that checks permission for user that perform action
 */
@Component
public class PermissionHandler {

	/**
	 * checks if category is allowed (order of category in binary representation is 1)
	 * @param type information type category
	 * @return true if has access, false otherwise
	 */
	public boolean hasAccessToInformationType(InformationType type) {
		JwtUser jwtUser = getCurrentUser();
		String categoriesAccessList = Integer.toBinaryString(jwtUser.getInformationTypes());
		return categoriesAccessList.charAt(type.getOrder()) == '1';
	}

	/**
	 * checks if user has access to specified product family
	 * @param productFamily product family
	 * @return true if has access, false otherwise
	 */
	public boolean hasAccessToProductFamily(ProductFamily productFamily) {
		JwtUser jwtUser = getCurrentUser();
		return jwtUser.getProductFamilies().contains(productFamily.name()) && !jwtUser.getProductFamiliesExclusions().contains(productFamily.name());
	}

	/**
	 * checks if user has access to specified product model and there are no exclusions
	 * @param productFamily product family
	 * @param modelId model id
	 * @return true if has access, false otherwise
	 */
	public boolean hasAccessToProductModel(ProductFamily productFamily, String modelId) {
		JwtUser jwtUser = getCurrentUser();
		boolean result = false;
		if (!jwtUser.getProductModelsExclusions().contains(modelId)) {
			//model is not in exclusions list
			if (jwtUser.getProductModels().contains(modelId)) {
				//model specified in access list
				result = true;
			} else if (jwtUser.getProductFamilies().contains(productFamily.name()) && !jwtUser.getProductFamiliesExclusions().contains(productFamily.name())) {
				//product family is specified in access list
				result = true;
			}
		}
		return result;
	}


	/**
	 * checks if user has access to specified product prefix and there are no exclusions on that prefix or model
	 * @param productFamily product family
	 * @param modelId product model
	 * @param prefix prefix
	 * @return true if has access false otherwise
	 */
	public boolean hasAccessToProductPrefix(ProductFamily productFamily, String modelId, String prefix) {
		JwtUser jwtUser = getCurrentUser();
		boolean result = false;
		if (!jwtUser.getProductPrefixes().contains(prefix)) {
			//model is not in exclusions list
			if (jwtUser.getProductModels().contains(modelId) || !jwtUser.getProductModelsExclusions().contains(modelId)) {
				//model specified in access list
				result = true;
			} else if (jwtUser.getProductFamilies().contains(productFamily.name())
					&& !jwtUser.getProductModelsExclusions().contains(modelId)
					&& !jwtUser.getProductFamiliesExclusions().contains(productFamily.name())) {
				//product family is specified in access list, and there are no exclusions on model and family level
				result = true;
			}
		}
		return result;
	}

	/**
	 * returns current user from user session
	 * @return {@link JwtUser}
	 */
	private JwtUser getCurrentUser() {
		return (JwtUser) SecurityContextHolder
				.getContext()
				.getAuthentication()
				.getPrincipal();
	}
}
