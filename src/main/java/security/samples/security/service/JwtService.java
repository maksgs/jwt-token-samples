package security.samples.security.service;

import security.samples.Constants;
import security.samples.model.ProductFamily;
import security.samples.security.model.JwtToken;
import security.samples.security.model.JwtUser;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.base.Joiner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class JwtService {

	private static final String CWS_ID = "cwsId";
	private static final String INFORMATION_TYPES = "informationTypes";
	private static final String PRODUCT_FAMILIES = "productFamilies";
	private static final String PRODUCT_MODELS = "productModels";
	private static final String PRODUCT_PREFIXES = "productPrefixes";
	private static final String PRODUCT_FAMILIES_EXCLUSIONS = "productFamiliesExclusions";
	private static final String PRODUCT_MODELS_EXCLUSIONS = "productModelsExclusions";
	private static final String PRODUCT_PREFIXES_EXCLUSIONS = "productPrefixesExclusions";

	@Value("${jwt.secret}")
	private String secret;
	@Value("${jwt.expiration}")
	private Integer expirationSeconds;
	@Value("${jwt.refresh_token_expiration}")
	private Integer refreshTokenExpirationSeconds;

	/**
	 * Create new jwt token, that should be return on ui
	 * @param cwsId cws id
	 * @return {@link JwtUser}
	 */
	public JwtToken createJwtToken(String cwsId) {

		JwtUser jwtUser;
		//todo: pull data from DB
		jwtUser = new JwtUser(
				cwsId,
				Integer.MAX_VALUE,
				Arrays.asList(ProductFamily.ADT.name(), ProductFamily.AP.name(), ProductFamily.BHL.name()),
				Arrays.asList("1", "2"),
				new ArrayList<>(),
				new ArrayList<>(),
				Arrays.asList("3", "5"),
				Arrays.asList("bcd", "def")
				);
		JwtToken jwtToken = new JwtToken();
		jwtToken.setAccessToken(generateAccessToken(jwtUser));
		jwtToken.setRefreshToken(generateRefreshToken(cwsId));
		return jwtToken;
	}


	/**
	 * Tries to parse specified String as a JWT token. If successful, returns User object with username, id and role prefilled (extracted from token).
	 * If unsuccessful (token is invalid or not containing all required user properties), simply returns null.
	 *
	 * @param token the JWT token to parse
	 * @return the User email extracted from specified token or RuntimeException if a token is invalid.
	 */
	public JwtUser parseToken(String token) {
		DecodedJWT decodedJwt = getDecodedJwt(token);
		return new JwtUser(
				decodedJwt.getClaim(CWS_ID).asString(),
				decodedJwt.getClaim(INFORMATION_TYPES).asInt(),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_FAMILIES).asString().split(Constants.COMMA)),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_MODELS).asString().split(Constants.COMMA)),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_PREFIXES).asString().split(Constants.COMMA)),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_FAMILIES_EXCLUSIONS).asString().split(Constants.COMMA)),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_MODELS_EXCLUSIONS).asString().split(Constants.COMMA)),
				Arrays.asList(decodedJwt.getClaim(PRODUCT_PREFIXES_EXCLUSIONS).asString().split(Constants.COMMA))
				);
	}



	/**
	 * Get decoded jwt from token
	 *
	 * @param token token
	 * @return decoded jwt
	 */
	private DecodedJWT getDecodedJwt(String token) {
		Algorithm algorithm = getAlgorithm();
		JWTVerifier verifier = JWT.require(algorithm).build();
		return verifier.verify(token);

	}


	/**
	 * Generates a JWT token containing username as subject, and userId and role as additional claims.
	 * These properties are taken from the specified jwt user object. Tokens validity is specified by expirationSeconds.
	 *
	 * @param jwtUser Jwt user
	 * @return the JWT token
	 */
	private String generateAccessToken(JwtUser jwtUser) {
		Algorithm algorithm = getAlgorithm();
		Date expirationDate = new Date(new Date().getTime() + expirationSeconds * 1000);
		return JWT.create()
				.withClaim(CWS_ID, jwtUser.getCwsId())
				.withClaim(INFORMATION_TYPES, jwtUser.getInformationTypes())
				.withClaim(PRODUCT_FAMILIES, mapListToString(jwtUser.getProductFamilies()))
				.withClaim(PRODUCT_MODELS, mapListToString(jwtUser.getProductModels()))
				.withClaim(PRODUCT_PREFIXES, mapListToString(jwtUser.getProductPrefixes()))
				.withClaim(PRODUCT_FAMILIES_EXCLUSIONS, mapListToString(jwtUser.getProductFamiliesExclusions()))
				.withClaim(PRODUCT_MODELS_EXCLUSIONS, mapListToString(jwtUser.getProductModelsExclusions()))
				.withClaim(PRODUCT_PREFIXES_EXCLUSIONS, mapListToString(jwtUser.getProductPrefixesExclusions()))
				.withExpiresAt(expirationDate)
				.sign(algorithm);
	}

	private String mapListToString(List<?> list) {
		return Joiner.on(Constants.COMMA).join(list);
	}

	private String generateRefreshToken(String cwsId) {
		Algorithm algorithm = getAlgorithm();
		Date expirationDate = new Date(new Date().getTime() + refreshTokenExpirationSeconds * 1000);
		return JWT.create()
				.withClaim(CWS_ID, cwsId)
				.withExpiresAt(expirationDate)
				.sign(algorithm);
	}

	/**
	 * Get code algorithm
	 */
	private Algorithm getAlgorithm() {
		return Algorithm.HMAC256(secret);
	}

}

