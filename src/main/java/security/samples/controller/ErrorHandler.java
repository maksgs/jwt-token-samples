package security.samples.controller;

import security.samples.Constants;
import security.samples.model.ApplicationErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@RestControllerAdvice
@Slf4j
public class ErrorHandler extends ResponseEntityExceptionHandler {


	/**
	 * Handles illegal argument exceptions
	 *
	 * @param ex exception
	 * @return bad request with error description
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<ApplicationErrorResponse> handleIllegalArgumentException(IllegalArgumentException ex) {
		log.error("IllegalArgumentException error", ex);
		return new ResponseEntity<>(new ApplicationErrorResponse(ex), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handles access denied exceptions
	 *
	 * @param ex exception
	 * @return forbidden with error description
	 */
	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<ApplicationErrorResponse> handleAccessDeniedException(AccessDeniedException ex) {
		log.error("AccessDeniedException error", ex);
		return new ResponseEntity<>(new ApplicationErrorResponse(ex), HttpStatus.FORBIDDEN);
	}

	/**
	 * Handles validation errors
	 *
	 * @return bad request with error description
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
																  HttpHeaders headers,
																  HttpStatus status,
																  WebRequest request) {
		String messages = ex.getBindingResult()
				.getAllErrors()
				.stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage)
				.collect(Collectors.joining(Constants.COMMA));
		log.error("Validation error: {}", messages);
		return new ResponseEntity<>(new ApplicationErrorResponse(messages), status);
	}
}