package security.samples.controller;

import security.samples.security.model.JwtToken;
import security.samples.security.service.JwtService;
import security.samples.Constants;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Constants.API_PREFIX + Constants.NOUATH_PREFIX )
@AllArgsConstructor
public class AuthController {

	private JwtService jwtService;

	/**
	 * creates jwt token
	 * @param cwsId csw id
	 * @return {@link JwtToken}
	 */
	@GetMapping(value = "/token")
	public JwtToken getJwtToken(@RequestParam String cwsId) {
		return jwtService.createJwtToken(cwsId);
	}


}
