package security.samples.controller;

import security.samples.model.InformationType;
import security.samples.model.ProductFamily;
import security.samples.Constants;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Constants.API_PREFIX)
public class SecuritySampleController {

	/**
	 * returns 200 in case if user has access to information type, 403 otherwise
	 */
	@GetMapping(value = "/informationType/{type}")
	@PreAuthorize("@permissionHandler.hasAccessToInformationType(#type)")
	public void hasAccessToInformationType(@PathVariable InformationType type) {
		//do nothing
	}

	/**
	 * returns 200 in case if user has access to product family, 403 otherwise
	 */
	@GetMapping(value = "/product/{family}")
	@PreAuthorize("@permissionHandler.hasAccessToProductFamily(#family)")
	public void hasAccessToProductFamily(@PathVariable(value = "family") ProductFamily family) {
		//do nothing
	}

	/**
	 * returns 200 in case if user has access to product model, 403 otherwise
	 */
	@GetMapping(value = "/product/{family}/{modelId}")
	@PreAuthorize("@permissionHandler.hasAccessToProductModel(#family, #modelId)")
	public void hasAccessToProductFamily(@PathVariable ProductFamily family, @PathVariable String modelId) {
		//do nothing
	}

	/**
	 * returns 200 in case if user has access to product prefix, 403 otherwise
	 */
	@GetMapping(value = "/product/{family}/{modelId}/{prefix}")
	@PreAuthorize("@permissionHandler.hasAccessToProductPrefix(#family, #modelId, #prefix)")
	public void hasAccessToProductPrefix(@PathVariable ProductFamily family, @PathVariable String modelId, @PathVariable String prefix) {
		//do nothing
	}
}
