package security.samples;

public class Constants {

	public static final String API_PREFIX = "/api";
	public static final String NOUATH_PREFIX = "/noAuth";

	public static final String AUTHORIZATION_HEADER = "Authorization";
	public static final String COMMA = ",";
}
