package security.samples.config;

import security.samples.config.filter.CorsFilter;
import security.samples.config.filter.JwtTokenAuthenticationFilter;
import security.samples.Constants;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final JwtTokenAuthenticationFilter jwtTokenAuthenticationFilter;

	private final CorsFilter corsFilter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.addFilterBefore(jwtTokenAuthenticationFilter, BasicAuthenticationFilter.class)
				.addFilterBefore(corsFilter, JwtTokenAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(Constants.API_PREFIX + Constants.NOUATH_PREFIX + "/**").permitAll()
				.antMatchers(Constants.API_PREFIX + "/**").authenticated();
	}



}