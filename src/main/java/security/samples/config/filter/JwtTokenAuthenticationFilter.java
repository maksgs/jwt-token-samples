package security.samples.config.filter;

import security.samples.Constants;
import security.samples.security.model.JwtAuthentication;
import security.samples.security.model.JwtUser;
import security.samples.security.service.ErrorMessageBuilder;
import security.samples.security.service.JwtService;
import com.auth0.jwt.exceptions.TokenExpiredException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static security.samples.Constants.AUTHORIZATION_HEADER;
import static org.apache.logging.log4j.util.Strings.isNotEmpty;


/**
 * Jwt token before filter
 */
@Slf4j
@Component
@AllArgsConstructor
public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

	private final JwtService jwtService;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest,
									HttpServletResponse httpServletResponse,
									FilterChain filterChain) throws ServletException, IOException {
		if (ignoreRequest(httpServletRequest)) {
			doFilter(httpServletRequest, httpServletResponse, filterChain);
		} else {
			String token = httpServletRequest.getHeader(AUTHORIZATION_HEADER);
			if (isNotEmpty(token) ) {
				processRequest(httpServletRequest, httpServletResponse, filterChain, token);
			} else {
				processRequestWithNoToken(httpServletResponse);
			}
		}
	}

	/**
	 * Return 401 for request with no token
	 *
	 * @param httpServletResponse {@link HttpServletRequest}
	 */
	private void processRequestWithNoToken(HttpServletResponse httpServletResponse) throws IOException {
		log.warn(ErrorMessageConstant.NO_TOKEN);
		httpServletResponse.getWriter().write(ErrorMessageBuilder.getJsonErrorMessage(ErrorMessageConstant.NO_TOKEN));
		httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
	}

	/**
	 * Process general request
	 *
	 * @param httpServletRequest  {@link HttpServletRequest}
	 * @param httpServletResponse {@link HttpServletResponse}
	 * @param filterChain         {@link FilterChain}
	 * @param token               jwt token
	 */
	private void processRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain, String token) throws IOException, ServletException {
		JwtUser jwtUser;
		try {
			jwtUser = jwtService.parseToken(token);
			log.trace("Token is not expired for user {}", jwtUser.getCwsId());
			SecurityContextHolder.getContext().setAuthentication(new JwtAuthentication(jwtUser));
			doFilter(httpServletRequest, httpServletResponse, filterChain);
		} catch (TokenExpiredException tee) {
			log.warn("Token is expired.");
			httpServletResponse.getWriter().write(ErrorMessageBuilder.getJsonErrorMessage(ErrorMessageConstant.INVALID_TOKEN));
			httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
		}
	}


	/**
	 * ignores all Options requests
	 *
	 * @param httpServletRequest {@link HttpServletRequest}
	 * @return true ignore, false check
	 */
	private boolean ignoreRequest(HttpServletRequest httpServletRequest) {
		return httpServletRequest.getMethod().equalsIgnoreCase("options") || httpServletRequest.getRequestURI().contains(Constants.NOUATH_PREFIX);
	}


}
