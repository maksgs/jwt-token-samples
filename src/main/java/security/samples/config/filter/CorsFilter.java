package security.samples.config.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


@Component
public class CorsFilter extends OncePerRequestFilter {

	private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
	private static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
	private static final String ACCESS_CONTROL_ALLOW_METHODS_VALUE = "GET,POST,DELETE,PUT,OPTIONS";

	private static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
	private static final String ACCESS_CONTROL_ALLOW_HEADERS_VALUE = "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, " +
			"Origin, Content-Type, Content-Disposition, Content-Description";
	private static final String ACCESS_CONTROL_MAX_AGE = "Access-Control-Max-Age";
	private static final String ACCESS_CONTROL_MAX_AGE_VALUE = "180";
	private static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
	private static final String COMMA = ",";
	private static final String ORIGIN_HEADER = "origin";


	@Value("${cors.origins}")
	private String corsOrigins;

	@Override
	protected void doFilterInternal(HttpServletRequest httpServletRequest,
									HttpServletResponse httpServletResponse,
									FilterChain filterChain) throws ServletException, IOException {
		List<String> corsOriginsList = Arrays.asList(corsOrigins.split(COMMA));
		String origin = httpServletRequest.getHeader(ORIGIN_HEADER);
		if (corsOriginsList.contains(origin)) {
			httpServletResponse.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, origin);
		} else {
			httpServletResponse.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, corsOrigins);
		}
		httpServletResponse.setHeader(ACCESS_CONTROL_ALLOW_METHODS, ACCESS_CONTROL_ALLOW_METHODS_VALUE);
		httpServletResponse.setHeader(ACCESS_CONTROL_ALLOW_HEADERS, ACCESS_CONTROL_ALLOW_HEADERS_VALUE);
		httpServletResponse.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS, Boolean.TRUE.toString());
		httpServletResponse.setHeader(ACCESS_CONTROL_MAX_AGE, ACCESS_CONTROL_MAX_AGE_VALUE);
		doFilter(httpServletRequest, httpServletResponse, filterChain);
	}
}

