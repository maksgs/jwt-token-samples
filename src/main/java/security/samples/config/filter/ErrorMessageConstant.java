package security.samples.config.filter;

public class ErrorMessageConstant {

    public static final String NO_TOKEN = "Authentication failed - no token provided.";

    public static final String INVALID_TOKEN = "Token is not valid, please try to refresh your token.";

}
