package security.samples.config;

import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;
import java.time.Clock;

@Configuration
@EnableJpaRepositories
public class PersistenceConfig {

    /**@Value("${spring.datasource.url}")
    private String dataSourceURL;

    @Value("${spring.datasource.password}")
    private String dataSourcePassword;

    @Value("${spring.datasource.username}")
    private String dataSourceUsername;


    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource(){

        return DataSourceBuilder.create().url(dataSourceURL).username(dataSourceUsername).password(dataSourcePassword).build();

    }*/


}
