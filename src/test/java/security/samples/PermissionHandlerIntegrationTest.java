package security.samples;

import security.samples.config.filter.JwtTokenAuthenticationFilter;
import security.samples.model.InformationType;
import security.samples.model.ProductFamily;
import security.samples.security.model.JwtToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import static junit.framework.TestCase.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * This integration test runs context and execute rest controller to test methods in PermissionHandler, which is used in PreAuthorized annotations
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PermissionHandlerIntegrationTest {

	private static final String CWS_ID = "sample";

	@Autowired
	private WebApplicationContext wac;

	@Autowired
	private JwtTokenAuthenticationFilter filter;

	private MockMvc mockMvc;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(this.wac)
				.dispatchOptions(true)
				.addFilters(filter).build();
	}

	@Test
	public void testGetToken() throws Exception {
		JwtToken jwtToken = getJwtToken();
		assertTrue(jwtToken.getAccessToken().length() > 0);
		assertTrue(jwtToken.getRefreshToken().length() > 0);
	}

	@Test
	public void testSecurityInformationType() throws Exception {
		JwtToken jwtToken = getJwtToken();
		mockMvc.perform(MockMvcRequestBuilders.get("/api/informationType/" + InformationType.ONE.name())
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
	}

	@Test
	public void testSecurityProductFamily() throws Exception {
		JwtToken jwtToken = getJwtToken();
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/" + ProductFamily.ADT.name())
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());
		//request without header
		mockMvc.perform(get("/api/product/" + ProductFamily.ADT.name())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isUnauthorized());

		//access denied product family is not allowed
		mockMvc.perform(get("/api/product/" + ProductFamily.WHEX.name())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());
				//.andExpect(status().isForbidden());
	}

	@Test
	public void testSecurityProductModel() throws Exception {
		JwtToken jwtToken = getJwtToken();
		//has access to family
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/" + ProductFamily.ADT.name() + "/1")
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		//has access to model, but doesn't have access to family
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/" + ProductFamily.WHEX.name() + "/2")
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		//access denied doesn't have access
		mockMvc.perform(get("/api/product/" + ProductFamily.ADT.name() + "/3")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());

		//access denied override by model exclusion
		mockMvc.perform(get("/api/product/" + ProductFamily.WHEX.name() + "/4")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());
		//.andExpect(status().isForbidden());
	}

	@Test
	public void testSecurityProductPrefix() throws Exception {
		JwtToken jwtToken = getJwtToken();
		//has access to family
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/" + ProductFamily.ADT.name() + "/1/abc")
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		//has access to model, but doesn't have access to family
		mockMvc.perform(MockMvcRequestBuilders.get("/api/product/" + ProductFamily.WHEX.name() + "/2/abc")
				.header(Constants.AUTHORIZATION_HEADER, jwtToken.getAccessToken())
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		//access denied doesn't have access
		mockMvc.perform(get("/api/product/" + ProductFamily.ADT.name() + "/3/abc")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());

		//access denied override by model exclusion
		mockMvc.perform(get("/api/product/" + ProductFamily.WHEX.name() + "/4/abc")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());
		//.andExpect(status().isForbidden());

		//access denied override by prefix exclusion
		mockMvc.perform(get("/api/product/" + ProductFamily.ADT.name() + "/1/bcd")
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				//todo: check why status is 401 and not 403
				.andExpect(status().isUnauthorized());
		//.andExpect(status().isForbidden());
	}

	private JwtToken getJwtToken() throws Exception {
		//request jwt token
		ResultActions tokenResponse = mockMvc.perform(get("/api/noAuth/token?cwsId=" + CWS_ID)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		String tokenResponseString = tokenResponse.andReturn().getResponse().getContentAsString();
		return objectMapper.readValue(tokenResponseString, JwtToken.class);
	}

}